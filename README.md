#Overview

###This source code demonstrates the usage of displaying child record below to respective parent record in sales-force.


Before Start with actually code part, i need something to mention about library i have used develop this visual-force page,

+ Bootstrap.css
+ Jquery.js

###Code example :

Now Start with Creating the **parentToChild** Controller 

```
#!Apex Controller
global with sharing class parentToChild {
  
}

```

+ I displaying all the account in the form of table, So i am using **@RemoteAction** (accountList()) to get all the account detail from the apex class **parentToChild**. 
+ **@RemoteAction**(contactList()) for fetching contact list, when an account name is clicked. **accId** is an **account id** is passed when account name is clicked. 

Below is controller after creating **@remoteAction** function in apex class.

```
#!Apex Controller
global with sharing class parentToChild {
    public parentToChild(){}
        
    @RemoteAction
    global static List<account> accountList(){
       return [select id,name from account limit 20];
    }
    @RemoteAction
    global static List<contact> contactList(String accId){
       return [select id,FirstName,LastName from contact where accountId=:accId];       
    }
}

```

##Now coming to Visualforce Page .

+ First add the basic necessary  for the visualforce page. Enable the **Html5.0** and disable the sidebar and header of the salesforce page for full screen preview .
+ Disable the **salesforce standard stylesheet** also. 
+ Add **bootstrap** and **jquery** files, either you can include the bootstrap and jquery in static resource and including by calling static Resource name also or directly what i have did. Always i prefer to use static Resource.


```
#! Visualforce Page 
<apex:page docType="html-5.0" controller="parentToChild" sidebar="false" showHeader="false" standardStylesheets="false" >
    <head>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
        <apex:includeScript value="https://code.jquery.com/jquery-3.2.1.min.js" />
        <script>
        j$ = jQuery.noConflict();           
        </script>
    </head>
    

    </apex:page>

```

+ Now display the all the account list in visual force page. 
+ when page is load, in the bottom of the page , i am calling javascript script function **loadAccountList()**  which internally call the apex **@RemoteAction** function.
+ As a result it will send all the account list from the org. From result i am fetching each record and append the result to **#accountTable** to his table body, as table row with column as **Account Id** and **Account Name**.


```
<apex:page docType="html-5.0" controller="parentToChild" sidebar="false" showHeader="false" standardStylesheets="false" >
    <head>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
        <apex:includeScript value="https://code.jquery.com/jquery-3.2.1.min.js" />
        <script>
        j$ = jQuery.noConflict();           
        </script>
    </head>
    
    <script>

    function loadAccountList() {
        Visualforce.remoting.Manager.invokeAction(
            '{!$RemoteAction.parentToChild.accountList}',
            function(result, event) {
                if (event.status) {                    
                    console.log('result' + result);
                    var htmlTag="";
                    for (var i = 0; i < result.length; i++)                        
                        htmlTag+="<tr id=\""+ result[i].Id +"\"><td>" + result[i].Id  + 
                            "</td> <td><a href=\"javascript:void(0);\" onclick=\"loadContactlist('" +  
                            result[i].Id +"');\"  > " + result[i].Name + "</a></td></tr>";                       
                    jQuery("#tbody").append(jQuery(htmlTag));
                }
            }, {
                escape: true
            }
        );
    }
    
    </script>
    <div class="container">
        <table id="accountTable" class="table table-bordered">
            <thead>
                <tr>
                    <th>Account Id</th>
                    <th>Account Name</th>
                </tr>
            </thead >
            <tbody id="tbody">
                
            </tbody>
        </table>
    </div>
    <script>  loadAccountList();</script>
    </apex:page>
```


* Now finally one thing i am left is ,When i am click on account name, Corresponding contact list of the account should be display exactly below the account name.
+ While adding account to the page,i have used **account id** as **Table row(<tr>) ID**. With help of this i can identify on below which row i have to show the contact list.
+ When user click on account Name, correctponding ** account Id** is send to **loadContactlist()**, and **loadcontact()** list calls **apex @RemoteAction ** function. As a result it will return the child records ie.. contact list.
+ With the help of jquery i have create the table for displaying contact list and add the contact table exactly next to the clicked account name  ** jQuery("#" + accId).after(htmlTrtag); **
+ If you click on the account name again if it is displaying child record already, then its remove the child record from the page.

```
<apex:page docType="html-5.0" controller="parentToChild" sidebar="false" showHeader="false" standardStylesheets="false" >
    <head>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
        <apex:includeScript value="https://code.jquery.com/jquery-3.2.1.min.js" />
        <script>
        j$ = jQuery.noConflict();           
        </script>
    </head>
    
    <script>

    function loadAccountList() {
        Visualforce.remoting.Manager.invokeAction(
            '{!$RemoteAction.parentToChild.accountList}',
            function(result, event) {
                if (event.status) {                    
                    console.log('result' + result);
                    var htmlTag="";
                    for (var i = 0; i < result.length; i++)                        
                        htmlTag+="<tr id=\""+ result[i].Id +"\"><td>" + result[i].Id  + 
                            "</td> <td><a href=\"javascript:void(0);\" onclick=\"loadContactlist('" +  
                            result[i].Id +"');\"  > " + result[i].Name + "</a></td></tr>";                       
                    jQuery("#tbody").append(jQuery(htmlTag));
                }
            }, {
                escape: true
            }
        );
    }
    
    function loadContactlist(accId) {
        Visualforce.remoting.Manager.invokeAction(
            '{!$RemoteAction.parentToChild.contactList}',accId,
            function(result, event) {
                if (event.status) {                    
                    console.log(result);
                    
                    if(! jQuery("#tr_" + accId).length && result.length){  
                        
                        var htmlTdtag = jQuery("<td colspan=\"2\"></td>"); 
                        var htmlTrtag = jQuery("<tr id=\"tr_"+ accId  +"\"></tr>");
                        
                        var htmlTableTag=jQuery("<table class=\"table table-bordered\"></table>");
                        var htmlTheadTag=jQuery("<thead><tr><th>First Name</th><th>LastName</th></tr></thead>");
                        var htmlTbodyTag=jQuery("<tbody></tbody>");
                        
						var contactListHtmlTag="";                        
                        for (var i = 0; i < result.length; i++) 
                            contactListHtmlTag+="<tr><td> "+ result[i].FirstName  +"</td><td> "+ result[i].LastName + "</td></tr>";
                      
                        jQuery(htmlTbodyTag).append(jQuery(contactListHtmlTag));  
                        
                         jQuery(htmlTableTag).append(htmlTheadTag);
                         jQuery(htmlTableTag).append(htmlTbodyTag);
                         jQuery(htmlTdtag).append(htmlTableTag);
                         jQuery(htmlTrtag).append(htmlTdtag);  
                         jQuery("#" + accId).after(htmlTrtag);                              
                    } //if 
                    else {
                        jQuery("#tr_" + accId).remove();
                    }
                }
            }, {
                escape: true
            }
        );
    }
    
    </script>
    <div class="container">
        <table id="accountTable" class="table table-bordered">
            <thead>
                <tr>
                    <th>Account Id</th>
                    <th>Account Name</th>
                </tr>
            </thead >
            <tbody id="tbody">
                
            </tbody>
        </table>
    </div>
    <script>  loadAccountList();</script>
    </apex:page>

```

##Screen Shot
![Screen Shot](/DisplayChildRecord/parentToChild.png)



[Question was asked in StackExchange about same topic](https://salesforce.stackexchange.com/questions/197504/display-child-record-when-click-on-parent-record-in-vf-pageblocktable?noredirect=1#comment298472_197504)