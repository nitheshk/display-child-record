global with sharing class parentToChild {
    public parentToChild (){}
        
    @RemoteAction
    global static List<account> accountList(){
       return [select id,name from account limit 20];
    }
    @RemoteAction
    global static List<contact> contactList(String accId){
       return [select id,FirstName,LastName from contact where accountId=:accId];       
    }
}